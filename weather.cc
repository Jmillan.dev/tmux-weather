#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <fstream>
#include <map>
#include <memory>
#include <stdexcept>
#include <string>

#include <curl/curl.h>
#include "json.hpp"


const long int call_delay    = 60 /* sec to minute */ * 10 /* 10 minutes */;
const long int display_delay = 60; // per second


template<typename ... Args>
std::string string_format(const std::string& format,
                          Args ... args)
{
    size_t size = snprintf( nullptr, 0, format.c_str(), args ... ) + 1; // Extra space for '\0'
    if( size <= 0 ){ throw std::runtime_error( "Error during formatting." ); }
    std::unique_ptr<char[]> buf( new char[ size ] );
    snprintf( buf.get(), size, format.c_str(), args ... );
    return std::string( buf.get(), buf.get() + size - 1 ); // We don't want the '\0' inside
}


bool string_starts_with(const std::string &src,
                        const std::string &format)
{
    int size = format.size();
    return src.substr(0, size) == format;
}


static int writer(char *data,
                  size_t size,
                  size_t nmemb,
                  std::string *buffer)
{
    if (buffer != nullptr)
    {
        buffer->append(data, size * nmemb);
        return size *nmemb;
    }

    return 0;
}


static std::string read_api(std::string url)
{
    CURL *curl;
    CURLcode res;

    curl = curl_easy_init();

    if(curl)
    {
        std::string buffer;
        struct curl_slist *headers = nullptr;

        headers = curl_slist_append(headers, "Accept: application/json");
        headers = curl_slist_append(headers, "Content-Type: application/json");
        headers = curl_slist_append(headers, "charset: utf-8");

        curl_easy_setopt(curl, CURLOPT_HTTPHEADER,    headers);
        curl_easy_setopt(curl, CURLOPT_URL,           url.c_str());
        curl_easy_setopt(curl, CURLOPT_HTTPGET,       1);
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writer);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA,     &buffer);

        res = curl_easy_perform(curl);

        if (res == CURLE_OK)
        {
            char *ct = nullptr;

            if((res = curl_easy_getinfo(curl, CURLINFO_CONTENT_TYPE, &ct)) != CURLE_OK)
                return "";

            if ((CURLE_OK == res) && ct)
                return buffer;
        }

        curl_slist_free_all(headers);

        curl_easy_cleanup(curl);
    }
    else fputs("Failed to initialize curl", stderr);

    return "";
}


struct Weather
{
    std::string name;
    float       temp;

    Weather() : name(""), temp(0.0f)
    {}
};


inline int gets(int l, const std::string &k, const std::string &s, std::string &b)
{
    b = s.substr(k.size() + 1);
    if (b.empty())
    {
        fprintf(stderr, "%d:%s (no value was found)", l, k.c_str());
        return 0;
    }
    return 1;
}


struct WeatherData
{
    std::time_t                   last_call = 0;
    std::time_t                   last_display = 0;
    std::string                   api_key;
    int                           current_id;
    std::map<int, struct Weather> weathers;

    int load_configuration(const char *filename)
    {
        std::fstream file(filename, std::ios::in /*"r"*/);
        if (!file.is_open())
        {
            fprintf(stderr, "Could not open the file '%s'", filename);
            return 1;
        }

        std::string buffer;
        std::string value;

        for(int line = 0; !file.eof(); line++)
        {
            std::getline(file, buffer);
            if (buffer.empty())
                continue;

            if(string_starts_with(buffer, "API_KEY "))
            {
                if (!api_key.empty()) { fprintf(stderr, "%d:API_KEY (already set!)", line); return 0; }

                if (gets(line, "API_KEY", buffer, api_key) == 0) return 0;
            }
            else if (string_starts_with(buffer, "CITY_ID "))
            {
                std::string value;

                if (gets(line, "CITY_ID", buffer, value))
                    weathers[std::stoi(value.c_str(), nullptr, 10)] = Weather();
            }

            line++;
        }

        file.close();

        if (api_key.empty()) return 1;

        return 0;
    }


    /* Reads form a file:
     *  - update time
     *  - display time
     *  - and every weather data collected
     *
     * returns:
     * 0 on success;
     * 1 in failure;
     */
    int read_data(const char *filename)
    {
        // reset timers
        last_call = std::time(nullptr);
        last_display = std::time(nullptr);

        std::fstream file(filename, std::ios::in /*"r"*/);
        if (!file.is_open())
            return 1;

        std::string buffer;

        enum E_STATE { CALL, DISPLAY, LAST_ID, WEATHER_ID, WEATHER_NAME, WEATHER_TEMP };

        int state = CALL; /* what value are we searching for */
        int weather_id = -1;

        // restart the values in timers
        last_call = 0;
        last_display = 0;

        while(!file.eof())
        {
            // copy it into the std::string for easy handling.
            std::getline(file, buffer);

            if (buffer.empty()) // ignore empty lines
                continue;

            // we trust that we don't get out of the enum values...
            // TODO add a safe fail for reading the information in the file
            switch(state)
            {
                case CALL:
                    last_call = std::stol(buffer.c_str(), nullptr, 10);
                    state = DISPLAY;
                    break;
                case DISPLAY:
                    last_display = std::stol(buffer.c_str(), nullptr, 10);
                    state = LAST_ID;
                    break;
                case LAST_ID:
                    current_id = std::stoi(buffer.c_str(), nullptr, 10);
                    state = WEATHER_ID;
                    break;
                case WEATHER_ID:
                    weather_id = std::stoi(buffer.c_str(), nullptr, 10);
                    state = WEATHER_NAME;
                    break;
                case WEATHER_NAME:
                    weathers[weather_id].name = buffer;
                    state = WEATHER_TEMP;
                    break;
                case WEATHER_TEMP:
                    weathers[weather_id].temp = std::stof(buffer.c_str(), nullptr);
                    state = WEATHER_ID;
                    break;
            }
        }

        file.close();

        return 0;
    }

    int write_data(const char *filename)
    {
        FILE *file = std::fopen(filename, "w");

        fprintf(file, "%ld\n", last_call);
        fprintf(file, "%ld\n", last_display);
        fprintf(file, "%d\n",  current_id);

        for(const auto& [id, data] : weathers)
        {
            if (!data.name.empty())
            {
                fprintf(file, "%d\n", id);
                fprintf(file, "%s\n", data.name.c_str());
                fprintf(file, "%f\n", data.temp);
            }
        }

        std::fclose(file);
        return 0;
    }

    int print_weather(void)
    {
        if (weathers.count(current_id))
        {
            auto weather = weathers[current_id];
            std::printf("%s: %.2fc\n", weather.name.c_str(), weather.temp);
            return 0;
        }
        else
        {
            fputs("No weather was found\n", stderr);
            return 1;
        }
    }
};


int main(void)
{
    const char conf_filename[] = "/etc/weather/conf";
    const char tmp_filename[] = "/tmp/weather";

    struct WeatherData w_data;

    if(w_data.load_configuration(conf_filename))
        return EXIT_FAILURE;

    w_data.read_data(tmp_filename);

    // read from the api if we can of course

    std::time_t now = std::time(nullptr);

    if (now - call_delay >= w_data.last_call)
    {
        w_data.last_call = now;
        for (auto& [id, weather] : w_data.weathers)
        {
            std::string url = string_format(
                "api.openweathermap.org/data/2.5/weather?id=%d&appid=%s&units=Metric",
                id, w_data.api_key.c_str()
            );

            std::string value = read_api(url);

            if (value.empty())
            {
                fputs("no data.", stderr);
                continue;
            }

            // parse our json data.
            nlohmann::json j_data = nlohmann::json::parse(value.c_str());

            weather.name = j_data["name"].get<std::string>();
            weather.temp = j_data["main"]["temp"].get<float>();
        }
    }

    // If we don't find it set the first one as the current id
    if (!w_data.weathers.count(w_data.current_id))
    {
        w_data.current_id = w_data.weathers.begin()->first;
    }

    // Rotate the id
    if (now - display_delay >= w_data.last_display)
    {
        auto iter = w_data.weathers.find(w_data.current_id);

        // move iter to the next value
        iter = std::next(iter, 1);

        // check if we are out of bounds
        if (iter == w_data.weathers.end())
            iter = w_data.weathers.begin();

        w_data.current_id = iter->first;
        w_data.last_display = now;
    }

    w_data.print_weather();

    w_data.write_data(tmp_filename);

    return EXIT_SUCCESS;
}
